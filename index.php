<html>
    <head>
        <!-- Bootstrap core CSS-->
        <link href="_vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom fonts for this template-->
        <link href="_vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <!-- Custom styles for this template-->
        <link href="_resource/css/sb-admin.css" rel="stylesheet">
    </head>
    <body class="fixed-nav sticky-footer bg-dark" id="page-top">
        <div id='errorMessageRow' style='width:100% !important; background:#FF9999; text-align:center; padding:10px; display:none;'>
            <label id='errorMessage'></label>
        </div>
    </body>

    <!-- Bootstrap core JavaScript-->
    <script src="_vendor/jquery/jquery.min.js"></script>
    <script src="_vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="_vendor/jquery-easing/jquery.easing.min.js"></script>
</html>

<?php 
//require '_include/autoload.php'; 
//$AutoLoad = new AutoLoad("/");

$websiteName = "ACY Transport Corporation";
if(!empty($_GET['p'])) {
    switch($_GET['p']) {
        case $_GET['p'] = "login":
            include "views/login.php";
        break;
        case $_GET['p'] = "trips":
            include "model/trips.php";
            include "views/trips.php";
        break;
        case $_GET['p'] = "employees":
            include "model/employees.php";
            include "views/employees.php";
        break;
        case $_GET['p'] = "trucks":
            include "model/trucks.php";
            include "views/trucks.php";
        break;
        case $_GET['p'] = "routes":
            include "model/routes.php";
            include "views/routes.php";
        break;
    }
} else {
    // default page is log in
    include "views/login.php";
}
?>
