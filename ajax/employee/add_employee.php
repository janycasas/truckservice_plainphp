<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();

// autoload
require_once dirname(__FILE__).'/../../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . "/../../");

// get variables
$first_name = (isset($_REQUEST['first_name']))?$_REQUEST['first_name']:"";
$last_name  = (isset($_REQUEST['last_name']))?$_REQUEST['last_name']:"";
$role       = (isset($_REQUEST['role']))?$_REQUEST['role']:"";

// data array
$data = array();
array_push($data, $first_name, $last_name, $role);

$DbEmployees = new DbEmployees;
$result = $DbEmployees->insertRow($data);

if($result) {
    $return = array("type" => "success",
                    "description" => "Added");
} else {
    $return = array("type" => "error",
                    "description" => "Could not add to Database. Let Boss Know");
}

echo json_encode($return);
?>