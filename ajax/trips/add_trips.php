<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();

// autoload
require_once dirname(__FILE__).'/../../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . "/../../");

// get variables
$route  = (isset($_REQUEST['route']))?intval($_REQUEST['route']):"";
$truck  = (isset($_REQUEST['truck']))?intval($_REQUEST['truck']):"";
$driver = (isset($_REQUEST['driver']))?intval($_REQUEST['driver']):"";
$helper = (isset($_REQUEST['helper']))?$_REQUEST['helper']:"";

// data array
$data = array();
array_push($data, $route, $truck, $driver, $helper);

$DbTrips = new DbTrips;
$result = $DbTrips->insertRow($data);

if($result) {
    $return = array("type" => "success",
                    "description" => "Added");
} else {
    $return = array("type" => "error",
                    "description" => "Could not add to Database. Let Boss Know");
}

echo json_encode($return);
?>