<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();

// autoload
require_once dirname(__FILE__).'/../../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . "/../../");

// get variables
$cell_number    = (isset($_REQUEST['cell_number']))?$_REQUEST['cell_number']:"";
$from           = (isset($_REQUEST['from']))?$_REQUEST['from']:"";
$to             = (isset($_REQUEST['to']))?$_REQUEST['to']:"";

// data array
$data = array();
$data["cell_number"] = $cell_number;
$data["from"]        = $from;
$data["to"]          = $to;

try {
    $DbRoutes   = new DbRoutes;
    $result     = $DbRoutes->insertRow($data);

    $return = array("type" => "success",
                    "description" => "Added");
} catch (Exception $e) {
    $return = array("type" => "error",
                    "description" => "ERROR: " . $e->getMessage());
}

echo json_encode($return);
?>