<?php 
error_reporting(E_ALL);
ini_set('display_errors', '1');

session_start();

// autoload
require_once dirname(__FILE__).'/../../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . "/../../");

// get variables
$type            = (isset($_REQUEST['type']))?$_REQUEST['type']:"";
$plate_number    = (isset($_REQUEST['plate_number']))?$_REQUEST['plate_number']:"";
$body_number     = (isset($_REQUEST['body_number']))?$_REQUEST['body_number']:"";
$volume_capacity = (isset($_REQUEST['volume_capacity']))?$_REQUEST['volume_capacity']:"";

// data array
$data = array();
array_push($data, $type, $plate_number, $body_number, $volume_capacity);

var_dump($data);
$DbTrucks = new DbTrucks;
$result = $DbTrucks->insertRow($data);

if($result) {
    $return = array("type" => "success",
                    "description" => "Added");
} else {
    $return = array("type" => "error",
                    "description" => "Could not add to Database. Let Boss Know");
}

echo json_encode($return);
?>