<?php 
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

session_start();

// autoload
require_once dirname(__FILE__).'/../../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . "/../../");

// get variables
$username = (isset($_REQUEST['username']))?$_REQUEST['username']:"";
$password = (isset($_REQUEST['password']))?$_REQUEST['password']:"";

$db = new Database("truck_service");
$sql = "SELECT * 
        FROM users 
        WHERE   username = '" . $username . "'
            AND password = '" . $password . "'
        LIMIT 1";
$user = $db->selectQuery($sql)[0];

if($user) {
    // set the session
    $_SESSION["logged_in"]  = $user["id"];
    $_SESSION["name"]       = $user["name"];

    $return = array("type" => "success",
                    "description" => "Logged In",
                    "homepage" => $user["homepage"]);
} else {
    $return = array("type" => "error",
                    "description" => "Username or Password is wrong");
}

echo json_encode($return);

?>