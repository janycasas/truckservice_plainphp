<?php include "views/navegation.php"; ?>

<title><?=$websiteName?> - <?=$pageTitle?></title>

<div class="content-wrapper">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            View Routes
                        </button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Cell Number</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Functions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($routes as $route) { ?>
                                        <tr>
                                            <td><?=$route["cell_number"]?></td>
                                            <td><?=$route["from"]?></td>
                                            <td><?=$route["to"]?></td>
                                            <td>Edit / Delete</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Add Routes
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <div class="form-group">
                            <div>
                                Cell Number:
                            </div>
                            <input type='text' class='form-control' id='cell_number'>
                        </div>
                        <div class="form-group">
                            <div>
                                From:
                            </div>
                            <input type='text' class='form-control' id='from'>
                        </div>
                        <div class="form-group">
                            <div>
                                To:
                            </div>
                            <input type='text' class='form-control' id='to'>
                        </div>
                        <button class="btn btn-primary btn-block" id="createRoute">Create Route</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("body").on("click", "#createRoute", function () {
	var formData = new FormData();
    formData.append('cell_number', $("#cell_number").val());
    formData.append('from', $("#from").val());
    formData.append('to', $("#to").val());

	$.ajax({
		url: "ajax/routes/add_route.php",
		type: "POST",
		data: formData,
		contentType: false,
		cache: false,
		processData: false
	}).done(function (data) {
        console.log(data);
        var parsedData = $.parseJSON(data);
        if(parsedData.type == "error") {
            $('#errorMessageRow').css("display","block");
            $("#errorMessage").html(parsedData.description);
            
            setTimeout(function () {
                $('#errorMessageRow').css("display","none");
				$('#errorMessage').html(" ");
			}, 3000);
        } else {

        }
	});
});
</script>
