<?php include "views/navegation.php"; ?>

<title><?=$websiteName?> - <?=$pageTitle?></title>

<div class="content-wrapper">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            View Employees
                        </button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Roles</th>
                                        <th>Functions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach($employees as $employee) { 
                                            $assignedRole = "";
                                            $roles = explode(",", $employee["roles"]);
                                            foreach($roles as $role) {
                                                $curRole = $DbEmployeesRoles->getSpecific($role);
                                                $assignedRole .= $curRole["role"] . "<br/>";
                                            }
                                    ?>
                                        <tr>
                                            <td><?=$employee["first_name"]?></td>
                                            <td><?=$employee["last_name"]?></td>
                                            <td><?=$assignedRole?></td>
                                            <td>Edit / Delete</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Add Employees
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <div class="form-group">
                            <div>
                                First Name:
                            </div>
                            <input type='text' class='form-control' id='first_name'>
                        </div>
                        <div class="form-group">
                            <div>
                                Last Name:
                            </div>
                            <input type='text' class='form-control' id='last_name'>
                        </div>
                        <div class="form-group">
                            <div>
                                Roles:
                            </div>
                            <?php foreach($possibleRoles as $role) { ?>
                                <input class='role' type="checkbox" name="role[]" value="<?=$role["id"]?>"> <?=$role["role"]?><br>
                            <?php } ?>
                        </div>
                        <button class="btn btn-primary btn-block" id="createEmployee">Create Employee</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("body").on("click", "#createEmployee", function () {
	var formData = new FormData();
    formData.append('first_name', $("#first_name").val());
    formData.append('last_name', $("#last_name").val());

    // get the values of the role that was selected
    var role = [];
    $('.role:checked').each(function() {
        role.push($(this).val());
    });
    formData.append('role', role);

	$.ajax({
		url: "ajax/employee/add_employee.php",
		type: "POST",
		data: formData,
		contentType: false,
		cache: false,
		processData: false
	}).done(function (data) {
        console.log(data);
        var parsedData = $.parseJSON(data);
        /*
        if(parsedData.type == "error") {
            $('#errorMessageRow').css("display","block");
            $("#errorMessage").html(parsedData.description);
            
            setTimeout(function () {
                $('#errorMessageRow').css("display","none");
				$('#errorMessage').html(" ");
			}, 3000);
        } else {

        }
        */
	});
});
</script>
