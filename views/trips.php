<?php include "views/navegation.php"; ?>

<div class="content-wrapper">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        View Trips
                        </button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <?php foreach($trips as $row) {
                                                foreach ($row as $key => $value) {
                                        ?>  
                                        <th><?=$key?></th>
                                        <?php } 
                                        } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($trips as $row) { ?>
                                        
                                    <?php } ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Add Trips
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <div class="form-group">
                            <div>
                                Routes:
                            </div>
                            <select id='route' name='route'>
                                <?php foreach($routes as $route) { ?>
                                    <option value='<?=$route["hidden_id"]?>'><?=$route["cell_number"]?> (<?=$route["from"]?> -> <?=$route["to"]?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div>
                                Trucks:
                            </div>
                            <select id='truck' name='truck'>
                                <?php foreach($trucks as $truck) { ?>
                                    <option value='<?=$truck["hidden_id"]?>'><?=$truck["type"]?> (<?=$truck["plate_number"]?>)</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div>
                                Drivers:
                            </div>
                            <select id='driver' name='driver'>
                                <?php foreach($drivers as $driver) { 
                                        $employee = $DbEmployee->getSpecific($driver["employee_id"]);
                                ?>
                                    <option value='<?=$employee["hidden_id"]?>'><?=$employee["first_name"]?> <?=$employee["last_name"]?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <div>
                                Helpers:
                            </div>
                            <?php foreach($helpers as $helper) { 
                                $employee = $DbEmployee->getSpecific($helper["employee_id"]);
                            ?>
                            <input class='helper' type="checkbox" name="helper[]" value="<?=$employee["hidden_id"]?>"> <?= $employee["first_name"]?> <?=$employee["last_name"]?><br>
                            <?php } ?>
                        </div>
                        <button class="btn btn-primary btn-block" id="createRoute">Create route</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("body").on("click", "#createRoute", function () {
	var formData = new FormData();
    formData.append('route', $("#route").val());
    formData.append('truck', $("#truck").val());
    formData.append('driver', $("#driver").val());

    // get the values of the helpers that was selected
    var helper = [];
    $('.helper:checked').each(function() {
        helper.push($(this).val());
    });
    formData.append('helper', helper);

	$.ajax({
		url: "ajax/trips/add_trips.php",
		type: "POST",
		data: formData,
		contentType: false,
		cache: false,
		processData: false
	}).done(function (data) {
        console.log(data);
        var parsedData = $.parseJSON(data);
        /*
        if(parsedData.type == "error") {
            $('#errorMessageRow').css("display","block");
            $("#errorMessage").html(parsedData.description);
            
            setTimeout(function () {
                $('#errorMessageRow').css("display","none");
				$('#errorMessage').html(" ");
			}, 3000);
        } else {

        }
        */
	});
});
</script>
