<?php include "views/navegation.php"; ?>

<title><?=$websiteName?> - <?=$pageTitle?></title>

<div class="content-wrapper">
    <div class="container-fluid">
        <div id="accordion">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            View Trucks
                        </button>
                    </h5>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>Type</th>
                                        <th>Body Number</th>
                                        <th>Plate Number</th>
                                        <th>Volume Capacity</th>
                                        <th>Functions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($trucks as $truck) { ?>
                                        <tr>
                                            <td><?=$truck["type"]?></td>
                                            <td><?=$truck["body_number"]?></td>
                                            <td><?=$truck["plate_number"]?></td>
                                            <td><?=$truck["volume_capacity"]?></td>
                                            <td>Edit / Delete</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            Add Trucks
                        </button>
                    </h5>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <div class="form-group">
                            <div>
                                Type:
                            </div>
                            <input type='text' class='form-control' id='type'>
                        </div>
                        <div class="form-group">
                            <div>
                                Plate Number:
                            </div>
                            <input type='text' class='form-control' id='plate_number'>
                        </div>
                        <div class="form-group">
                            <div>
                                Body Number:
                            </div>
                            <input type='text' class='form-control' id='body_number'>
                        </div>
                        <div class="form-group">
                            <div>
                                Volume Capacity:
                            </div>
                            <input type='text' class='form-control' id='volume_capacity'>
                        </div>
                        <button class="btn btn-primary btn-block" id="createTrucks">Create Trucks</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$("body").on("click", "#createTrucks", function () {
	var formData = new FormData();
    formData.append('type', $("#type").val());
    formData.append('plate_number', $("#plate_number").val());
    formData.append('body_number', $("#body_number").val());
    formData.append('volume_capacity', $("#volume_capacity").val());

	$.ajax({
		url: "ajax/trucks/add_truck.php",
		type: "POST",
		data: formData,
		contentType: false,
		cache: false,
		processData: false
	}).done(function (data) {
        console.log(data);
        var parsedData = $.parseJSON(data);
        /*
        if(parsedData.type == "error") {
            $('#errorMessageRow').css("display","block");
            $("#errorMessage").html(parsedData.description);
            
            setTimeout(function () {
                $('#errorMessageRow').css("display","none");
				$('#errorMessage').html(" ");
			}, 3000);
        } else {

        }
        */
	});
});
</script>
