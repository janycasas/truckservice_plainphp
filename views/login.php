<?php 
$pageTitle = "Log-In";

?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><?=$websiteName?> - <?=$pageTitle?></title>
    </head>
    <body class="bg-dark">
        <div class="container">
            <div class="card card-login mx-auto mt-5">
                <div class="card-header">
                    Login
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label>Username</label>
                        <input class="form-control" id="username" name="username" type="email" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input class="form-control" id="password" name="password" type="password" placeholder="Password">
                    </div>

                    <button class="btn btn-primary btn-block" id="logInButton">Log In</button>
                </div>
            </div>
        </div>
    </body>
</html>

<script>
$("body").on("click", "#logInButton", function () {
	var formData = new FormData();
    formData.append('username', $("#username").val());
    formData.append('password', $("#password").val());

	$.ajax({
		url: "ajax/login/verify_login.php",
		type: "POST",
		data: formData,
		contentType: false,
		cache: false,
		processData: false
	}).done(function (data) {
        var parsedData = $.parseJSON(data);
        if(parsedData.type == "error") {

            $('#errorMessageRow').css("display","block");
            $("#errorMessage").html(parsedData.description);
            
            setTimeout(function () {
                $('#errorMessageRow').css("display","none");
				$('#errorMessage').html(" ");
			}, 3000);
        } else {
            window.location.href = parsedData.homepage;
        }
	});
});
</script>