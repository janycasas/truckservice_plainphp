<?php
    function verifyString($data) {
        if(is_string($data)) {
            return "'" . $data . "'";
        }
        return $data;
    }

    function returnDbColumn($column) {
        return "`" . $column . "`";
    }
?>