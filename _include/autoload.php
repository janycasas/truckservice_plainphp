<?php 
class AutoLoad {
    private $path;

    // Fills in the credential array
    function __construct($path) {
        $this->path = $path;
        $this->loadFiles();
    }

    function loadFiles(){
        $folderFiles = array("_include/","database_objects/");
        foreach($folderFiles as $folder) {
            $curPath = $this->path . $folder;
            foreach (glob($curPath . "*.php") as $filename) {
                if(stristr($filename,"autoload.php") !== FALSE) {
                    continue;
                }            
                //echo $curPath . $filename . " NEXT ";
                require_once $filename;
            }
        }
    }
}
?>