<?php
//error_reporting(E_ALL);
//ini_set('display_errors', '1');

class Database {
    private $credentials;
    private $conn;

    // Fills in the credential array
    function __construct($databaseName) {
        $this->credentials = $this->getCredentials($databaseName);
        $this->conn =$this->connect();
    }

    // closes connection when database object
    function __destruct(){
        $this->close();
    }

    // connect to the database
    function connect(){
        $servername     = $this->credentials["servername"];
        $username       = $this->credentials["username"];
        $password       = $this->credentials["password"];
        $databaseName   = $this->credentials["databaseName"];

        $conn = new mysqli($servername, $username, $password, $databaseName) 
            or die("Connect Failed: " . $conn->error);
        
        return $conn;
    }

    // returns the appropriate credentials depending on the database name
    function getCredentials($databaseName){
        $credentials = array();
        $credentials["servername"] = "localhost";

        if($databaseName == "truck_service") {
            $username = "root";
            $password = "root";
        }
        $credentials["username"]        = $username;
        $credentials["password"]        = $password;
        $credentials["databaseName"]    = $databaseName;
        
        return $credentials;
    }

    // close connection
    function close() {
        $this->conn->close();
    }

    // query the database
    function runQuery($query) {
        // Query the database
        $result = $this->conn->query($query);

        return $result;
    }

    // select query
    function selectQuery($query) {
        $result = $this->runQuery($query);
        if($result == false) {
            return false;
        }
        while ($row = $result -> fetch_assoc()) {
            $rows[] = $row;
        }
        return $rows;
    }

    // insert a new row
    function insertQuery($query) {
        $result = $this->runQuery($query);
        return $result;
    }

}

?>