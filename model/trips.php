<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once dirname(__FILE__).'/../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . "/../");

$DbTrips    = new DbTrips;
$trips      = $DbTrips->getAll();

$DbRoutes   = new DbRoutes;
$routes     = $DbRoutes->getAll();

$DbTrucks   = new DbTrucks;
$trucks     = $DbTrucks->getAll();

$DbDrivers  = new DbDrivers;
$drivers    = $DbDrivers->getAll();

$DbHelpers  = new DbHelpers;
$helpers    = $DbHelpers->getAll();

$DbEmployee = new DbEmployees;

?>