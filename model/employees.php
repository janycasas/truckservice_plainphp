<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once dirname(__FILE__).'/../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . "/../");

$DbEmployee     = new DbEmployees;
$employees      = $DbEmployee->getAll();

$DbEmployeesRoles   = new DbEmployeesRoles;
$possibleRoles      = $DbEmployeesRoles->getAll();

$pageTitle = "Employees";
?>