<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once dirname(__FILE__).'/../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . "/../");

$DbRoutes   = new DbRoutes;
$routes  = $DbRoutes->getAll();

$pageTitle = "Routes";
?>