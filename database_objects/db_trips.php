<?php 
require_once dirname(__FILE__).'/../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . '/../');

class DbTrips extends DbBase{
    function __construct(){
        parent::__construct();
        $this->tableName = "trips";
        /*
        array_push($this->columns,  "routes_id", "trucks_id" , "drivers_id", 
                                    "helpers_id", "dispatched_time", "disposal_time");
        */
        array_push($this->columns,  "routes_id", "trucks_id" , 
                                    "drivers_id", "helpers_id");
    }
}
?>