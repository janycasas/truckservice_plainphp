<?php 
require_once dirname(__FILE__).'/../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . '/../');

class DbRoutes extends DbBase{
    function __construct(){
        parent::__construct();
        $this->tableName = "routes";
        array_push($this->insert_columns, "cell_number", "from" , "to");
    }

    function passCriteria($datas) { 
        $sql = "SELECT 1 
                FROM " . $this->tableName . " " . 
                "WHERE cell_number = " . verifyString($datas["cell_number"]);
        $result = $this->db->selectQuery($sql);

        if($result) { 
            throw new Exception('Cell Number Already In Database');
        }

        return true;
    }
}
?>