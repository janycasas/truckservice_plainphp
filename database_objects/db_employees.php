<?php 
require_once dirname(__FILE__).'/../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . '/../');

class DbEmployees extends DbBase {
    function __construct() {
        parent::__construct();
        $this->tableName = "employees";
        array_push($this->insert_columns, "first_name", "last_name" , "roles");
    }
}
?>