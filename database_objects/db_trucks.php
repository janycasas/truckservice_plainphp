<?php 
require_once dirname(__FILE__).'/../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . '/../');

class DbTrucks extends DbBase{
    function __construct(){
        parent::__construct();
        $this->tableName = "trucks";
        array_push($this->insert_columns,  "type", "plate_number" , 
                                            "body_number", "volume_capacity");
    }
}
?>