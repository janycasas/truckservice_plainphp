<?php 
require_once dirname(__FILE__).'/../_include/autoload.php';
$AutoLoad = new AutoLoad(dirname(__FILE__) . '/../');

class DbBase {
    protected $db;
    protected $tableName;
    protected $insert_columns;
    protected $edit_columns;

    function __construct(){
        $this->db = new Database("truck_service");
        $this->insert_columns = array();
        $this->edit_columns = array();
    }

    function getAll(){
        $sql = "SELECT *
                FROM " . $this->tableName;

        $result = $this->db->selectQuery($sql);
        return $result;
    }

    function getSpecific($id) {
        $sql = "SELECT *
                FROM " . $this->tableName . 
                " WHERE id = " . $id . 
                " LIMIT 1";

        $result = $this->db->selectQuery($sql);
        return $result[0];
    }

    function insertRow($datas) {
        $this->passCriteria($datas);

        $sql =  "INSERT INTO " . $this->tableName . " (";

        // add the insert_columns
        for($i = 0; $i < count($this->insert_columns); $i++) {
            $column = $this->insert_columns[$i];
            $sql .= returnDbColumn($column);
            if(($i + 1) < count($this->insert_columns)) {
                $sql .= ",";
            }
        }

        // add the values
        $sql .= ") VALUES (";
        foreach($datas as $data) {
            $data = verifyString($data);
            $sql .= $data;
            if(($i + 1) < count($datas)) {
                $sql .= ",";
            }
        }

        $sql .= ");";

        //$result = $this->db->insertQuery($sql);
        //return $result;
    }

}
?>